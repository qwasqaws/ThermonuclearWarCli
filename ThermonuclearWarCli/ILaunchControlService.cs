﻿using Optional;
using WarheadsApiWrapper;

namespace ThermonuclearWarCli
{
    internal interface ILaunchControlService
    {
        string LaunchCode { get; }
        IWarheads Api { get; }

        Option<bool> IsOnline();
        Option<LaunchResponse> Launch();
    }
}