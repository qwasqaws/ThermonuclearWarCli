using System;

namespace ThermonuclearWarCli
{
    /// <summary>
    /// Exits the program.
    /// </summary>
    internal class ExitCommand : ICommand
    {
        private IDisplay Display { get; }
        public Action Exit { get; }

        public string Name { get; } = "exit";

        public ExitCommand(IDisplay display, Action exit)
        {
            Display = display;
            Exit = exit;
        }

        public void Execute()
        {
            Display.TypeLine("A STRANGE GAME.");
            Display.TypeLine("THE ONLY WINNING MOVE IS");
            Display.TypeLine("NOT TO PLAY.");
            Console.WriteLine();
            Display.TypeLine("HOW ABOUT A NICE GAME OF CHESS?");
            Console.ReadKey(true);
            this.Exit();
        }
    }
}