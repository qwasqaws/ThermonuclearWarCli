using System;
using System.Threading;
using WarheadsApiWrapper;

namespace ThermonuclearWarCli
{
    /// <summary>
    /// Launches a nuke.
    /// </summary>
    internal class LaunchCommand : ICommand
    {
        private ILaunchControlService LaunchControl { get; }
        private IDisplay Display { get; }

        public string Name { get; } = "launch";

        public LaunchCommand(ILaunchControlService launchControl, IDisplay display)
        {
            LaunchControl = launchControl;
            Display = display;
        }

        public void Execute()
        {
            Display.TypeLine("CONFIRM? Y/N");

            var confirmationKey = Console.ReadKey(true);
            if (confirmationKey.Key == ConsoleKey.Y)
            {
                Console.Clear();

                var spacedLaunchCode = string.Join(" ", LaunchControl.LaunchCode.ToCharArray());

                Display.TypeLine("WOPR EXECUTION ORDER");
                Display.TypeLine("K36.948.3");
                Console.WriteLine();
                Display.TypeLine("PART ONE: R O N C T T L");
                Display.TypeLine("PART TWO: 07:20:35");
                Console.WriteLine();
                Display.TypeLine("LAUNCH CODE: " + spacedLaunchCode);
                Console.WriteLine();

                LaunchControl.Launch()
                    .Match(
                        result =>
                        {
                            var typeWithDelay = new Action<string, int>((text, delay) =>
                            {
                                Display.Type(text);
                                Thread.Sleep(delay);
                            });

                            var typeLineWithDelay = new Action<string, int>((text, delay) =>
                            {
                                Display.TypeLine(text);
                                Thread.Sleep(delay);
                            });

                            var rewriteLineWithDelay = new Action<string, int>((text, delay) =>
                            {
                                Console.CursorLeft = 0;
                                Console.Write(text);
                                Thread.Sleep(delay);
                            });

                            if (result.Result != LaunchResponseResult.Success)
                            {
                                Display.TypeLine(result.Message.ToUpper());
                                return;
                            }


                            typeLineWithDelay("LAUNCH ORDER CONFIRMED", 1000);
                            Console.WriteLine();

                            typeWithDelay("TARGET SELECTION:   ", 500);
                            typeLineWithDelay("       COMPLETE", 500);


                            typeWithDelay("TIME ON TARGET SEQUENCE:   ", 500);
                            typeLineWithDelay("COMPLETE", 500);

                            typeWithDelay("YIELD SELECTION:   ", 500);
                            typeLineWithDelay("        COMPLETE", 1000);
                            Console.WriteLine();

                            Display.TypeLine("ENABLE MISSILES");
                            Console.WriteLine("---------------");
                            Console.WriteLine();
                            Thread.Sleep(500);

                            rewriteLineWithDelay("T MINUS 05 SECONDS", 1000);
                            rewriteLineWithDelay("T MINUS 04 SECONDS", 1000);
                            rewriteLineWithDelay("T MINUS 03 SECONDS", 1000);
                            rewriteLineWithDelay("T MINUS 02 SECONDS", 1000);
                            rewriteLineWithDelay("T MINUS 01 SECONDS", 1000);

                            rewriteLineWithDelay("                  ", 0);
                            Console.CursorLeft = 0;
                            
                            Display.TypeLine(">> LAUNCH <<");
                            Console.WriteLine("------------");
                            Console.WriteLine();
                            Thread.Sleep(1000);

                            Display.TypeLine("WINNER: NONE");
                            
                        },
                        () => Display.TypeLine("ERROR"));
            }
        }
    }
}